import { shallowMount } from "@vue/test-utils";
import Loader from "@/components/Loader.vue";

describe("Loader.vue", () => {
  it("should render component", () => {
    const wrapper = shallowMount(Loader);
    expect(wrapper.find(".loader-ring").exists()).toBeTruthy();
  });
});
