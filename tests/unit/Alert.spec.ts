import { shallowMount } from "@vue/test-utils";
import Alert from "@/components/Alert.vue";

describe("Alert.vue", () => {
  it("should render component", () => {
    const alertText = "Hello World!";
    const wrapper = shallowMount(Alert, {
      slots: { default: alertText },
    });
    expect(wrapper.text()).toBe(alertText);
  });
});
