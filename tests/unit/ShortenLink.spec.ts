import { shallowMount, mount } from "@vue/test-utils";
import ShortenLink from "@/views/ShortenLink.vue";
import axios from "axios";
import flushPromises from "flush-promises";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

describe("ShortenLink.vue", () => {
  it("should render heading", () => {
    const wrapper = shallowMount(ShortenLink);
    expect(wrapper.find("h1").text()).toEqual("Shorten a link!");
  });

  it("should shorten link", async () => {
    const short_link = "short.com";
    const full_short_link = "https://short.com";
    mockedAxios.get.mockImplementationOnce(() =>
      Promise.resolve({
        data: {
          result: {
            short_link,
            full_short_link,
          },
        },
      })
    );
    const wrapper = mount(ShortenLink);
    wrapper.find("input[name=link]").setValue("https://vuejs.org/");
    wrapper.find("form").trigger("submit.prevent");
    await flushPromises();
    expect(mockedAxios.get).toBeCalledTimes(1);
    expect(mockedAxios.get).toBeCalledWith("https://api.shrtco.de/v2/shorten", {
      params: { url: "https://vuejs.org/" },
    });
    expect(wrapper.find(".short-link").exists()).toBeTruthy();
    expect(wrapper.find(".short-link a").text()).toEqual(short_link);
    expect(wrapper.find(".short-link a").attributes().href).toBe(
      full_short_link
    );
  });

  it("should handle shorten link error", async () => {
    const error = "This is not a valid URL, for more infos see shrtco.de/docs";
    mockedAxios.get.mockImplementationOnce(() =>
      Promise.reject({
        response: {
          data: {
            error,
          },
        },
      })
    );
    const wrapper = mount(ShortenLink);
    wrapper.find("input[name=link]").setValue("https://vuejs.org/");
    wrapper.find("form").trigger("submit.prevent");
    await flushPromises();
    expect(wrapper.find(".alert").exists()).toBeTruthy();
    expect(wrapper.find(".alert").text()).toEqual(error);
  });
});
