import { shallowMount } from "@vue/test-utils";
import Container from "@/components/Container.vue";

describe("Container.vue", () => {
  it("should render component", () => {
    const containerText = "Hello World!";
    const wrapper = shallowMount(Container, {
      slots: { default: containerText },
    });
    expect(wrapper.text()).toBe(containerText);
  });
});
